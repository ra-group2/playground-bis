# This file is a template, and might need editing before it works on your project.
FROM golang:1.23.1 AS builder

WORKDIR "/workspace"

COPY . .
RUN go build -v main.go
RUN ls -halF

CMD ["exec", "main"]

# FROM busybox:latest as builder
# FROM gitlab/gitlab-runner-helper:x86_64-f86890c6
# WORKDIR /my_dir

# FROM golang@sha256:3183b343d01a6e8c9bee7b7c4eb7208518e68dc0cdac8623e1575820342472ed AS builder

# # install ca-certificates to copy them into the container
# RUN apk --no-cache add ca-certificates

# # use a separate workspace to isolate the artifacts
# WORKDIR "/workspace"

# # copy the go modules and manifests to download the dependencies
# COPY "go.mod" "go.mod"

# # cache the dependencies before copying the other source files so that this layer won't be invalidated on code changes
# RUN go mod download -x

# # copy all other files into the image to work on them
# COPY "." "./"

# # build the statically linked binary from the go source files
# RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o example main.go


# FROM scratch

# # copy the raw binary into the new container
# COPY --from=builder "/workspace/example" "/example"

# # copy the users and groups for the nobody user and group
# COPY --from=builder "/etc/passwd" "/etc/passwd"
# COPY --from=builder "/etc/group" "/etc/group"

# # copy the certificate storage for web service authentication
# COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# # we run with minimum permissions as the nobody user
# USER nobody:nobody

# # just execute the raw binary without any wrapper
# ENTRYPOINT ["/example"]


# FROM busybox:latest as builder
# RUN apk add bash

# CMD ["/bin/bash"]

# FROM alpine:3.16

# RUN apk upgrade --no-cache
