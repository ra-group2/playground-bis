#!/bin/sh

generate_random_string() {
    openssl rand -base64 12 | tr -dc 'a-zA-Z0-9'
}

duration=60

start_time=$(date +%s)
end_time=$((start_time + duration))

while [ $(date +%s) -lt $end_time ]
do
    random_string=$(generate_random_string)
    echo "Random string generated: $random_string"
    # usleep 1000  # 1ms pause
done
