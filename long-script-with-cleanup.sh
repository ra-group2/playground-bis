#!/bin/sh

rm -f output.txt

cleanup() {
	echo "caught signal $1" | tee -a output2.txt
	sleep 3
	echo "exiting..." | tee -a output2.txt
	exit 0
}

trap 'cleanup "SIGTERM"' TERM INT QUIT

for i in $(seq 1 60); do
	echo "$i" | tee -a output2.txt
	sleep 1
done
