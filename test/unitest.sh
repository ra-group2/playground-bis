#!/bin/bash

python3 -m http.server &
pid=$!
# nohup /bin/sh -c 'python3 -m http.server &'
envsubst < "testpage.html" > "testpage_compiled.html"
sleep 10
kill -9 ${pid}
#python3 -m pytest --junitxml results.xml test_tracker.py
cat results.xml
