# chrome problems
# - mouseenter mouseleave missing
# - headless and non headless differences
# - media seeking not supported with python http.server

# start local server: python -m http.server
# start unittest with JUnit output: pytest --junitxml results.xml unittest.py

import os
import json
import time 
import unittest
from uuid import UUID
import statistics

from seleniumwire import webdriver
from selenium.webdriver.common.by import By

RETURNEDUID = 'ce986222-cafc-11eb-b8bc-0242ac130003'
TIMEOUT = 10 # seconds
ENDPOINTURL = os.getenv('ENDPOINT_SNIPPET') # 'technical-service.net/event'
USERAGENT = 'firefox'

def interceptor(request):
    # Block requests containing ENDPOINTURL 
    if ENDPOINTURL in request.url:
        request.create_response(
            status_code=200,
            headers={
                'Content-Type': 'application/json', 
                'Access-Control-Allow-Origin' : 'http://127.0.0.1:8000', 
                'Access-Control-Allow-Credentials' : 'true', 
                'Access-Control-Allow-Methods' : 'GET,POST,OPTIONS'},
            body=TrackerTest.uid  # Optional body
        )

class TrackerTest(unittest.TestCase):
    uid = RETURNEDUID

    def getMessage(self, labelslice):
        # return last request matching labelslice in its url

        for request in reversed(self.driver.requests):
            if labelslice in request.url:
                return request
        raise RuntimeError("Message origin '" + labelslice + "' not found in message stack.")

    def waitAndTest(self, eventname):
        # wait for a message from ENDPOINTURL

        try:
            msg = self.getMessage(ENDPOINTURL)    
            self.assertMessageConform(msg.body)
            self.assertMessagePresent(msg.body, eventname)
        except: 
            pass
        else:
            return

        self.driver.wait_for_request(ENDPOINTURL, TIMEOUT)
        msg = self.getMessage(ENDPOINTURL)
        self.assertMessageConform(msg.body)
        self.assertMessagePresent(msg.body, eventname)




    def assertMessagePresent(self, body, eventname):
        # checks if an event with name eventname is present in the message body
        data = json.loads(body)
        events = data["events"]
        
        for event in events:
            if event[0].lower() == eventname.lower():
                # timing correct?
                if isinstance(event[1], int) and event[1] <= data['time']:
                    # found 
                    return
        raise RuntimeError("Message '" + eventname + "' not found in message stack.")



    def assertMessageConform(self, body):
        # checks if the message is valid json and contains the field:
        # uid, sid, pid, time, events

        data = json.loads(body) # throws if json not conform

        assert 'uid' in data, "No user id within message body"
        assert 'sid' in data, "No session id within message body"
        assert 'pid' in data, "No page id within message body"
        assert 'time' in data, "No transmission time within message body"
        assert 'events' in data, "No events within message body"

        # check uuids
        if data['uid'] != '':
            UUID(data['uid'], version=4)
        UUID(data['sid'], version=4)
        UUID(data['pid'], version=4)
        
        if not isinstance(data['time'], int) or data['time'] < 0:
            raise RuntimeError("Message transmission time invalid.")

        if not isinstance(data['events'], list):
            raise RuntimeError("Message event list invalid.")

    def assertMessageSyntax(self, eventdata):
        msg = self.getMessage(ENDPOINTURL) 
        data = json.loads(msg.body)
        events = data["events"]
        
        for event in events:
            if event[0].lower() == eventdata[0].lower():  
                  if event[2:len(eventdata)] == eventdata[2:]:
                      # message found
                      return
        raise RuntimeError("Syntax of message '" + eventdata[0] + "' incorrect.")


    def waitAndTestRepeatedly(self, eventlist, count):
        for i in range(count):
            self.driver.wait_for_request(ENDPOINTURL, TIMEOUT)
            msg = self.getMessage(ENDPOINTURL)
            try:
                self.assertMessageConform(msg.body)
                for event in eventlist:
                    self.assertMessagePresent(msg.body, event)
            except:
                pass

            else:
                break
            
            del self.driver.requests
            if i==count-1:
                raise RuntimeError("Messages " + str(eventlist) + " not found in message stack.")        


    def setUp(self):
        if USERAGENT == 'chrome':
            #options = ChromeOptions()
            #options.headless = True
            #self.driver = webdriver.Chrome(options=options)
            raise RuntimeError("Please select firefox.")

        else: # 'firefox'
            options = webdriver.FirefoxOptions()
            options.headless = True
            self.driver = webdriver.Firefox(options=options)

        self.driver.request_interceptor = interceptor
        self.driver.get("http://127.0.0.1:8000/testpage_compiled.html")   

    def tearDown(self):
        #self.driver.close()
        pass        

    def test_transmission(self):

        self.driver.wait_for_request(ENDPOINTURL, TIMEOUT)
        self.waitAndTest("start")       

        # check if ua event is present
        self.assertMessagePresent(self.getMessage(ENDPOINTURL).body, "start")


        # check if ua event time is smaller 10000 (loaded fast enough)
        # TODO

        # check if new uid is present
        self.driver.implicitly_wait(5)
        self.assertEqual(self.driver.execute_script('return window._itracker.uid'), RETURNEDUID)

        # check transmission frequency
        time_list = []
        for i in range(5):
            self.driver.wait_for_request(ENDPOINTURL, TIMEOUT)
            time_list.append(time.time())
            del self.driver.requests
        time_list = [time_list[i] - time_list[i-1] for i in range(1,len(time_list))]
        mean = statistics.mean(time_list)
        stddev = statistics.stdev(time_list)

        self.assertGreater(mean, 0, "Tracker network transmission frequency is infinite.")
        self.assertGreater(stddev, 0, "Standard deviaton of message interval is equal to zero. Result to perfect to be true.")
        self.assertLess(mean, 2, "Tracker network transmission frequency too low.")
        self.assertLess(stddev, 1, "Tracker network transmission interval erratic.")

        # check empty uid messages
        # withdrawn consent -> uid answer == ""

        self.assertEqual(self.driver.execute_script('return window._itracker.paused'), False, "Tracker is not running.")

        TrackerTest.uid = ""
        self.driver.wait_for_request(ENDPOINTURL, TIMEOUT)

        self.assertEqual(self.driver.execute_script('return window._itracker.paused'), True, "Tracker has not been stopped.")

        TrackerTest.uid = RETURNEDUID 

        del self.driver.requests
        try:   
            self.driver.wait_for_request(ENDPOINTURL, TIMEOUT)
        except:
            # if this timeouts, we are okay
            pass
        else:
            raise RuntimeError('Tracker still active after receiving no user id.')

        self.driver.close()


    def test_all(self):
        self.driver.implicitly_wait(5)

        with self.subTest("test_initialization"):
            # check if page loaded
            self.assertEqual(self.driver.title, "Unit test", "Unit test document not loaded")        

            # check if tracker loaded
            self.assertEqual(self.driver.execute_script('return window._itracker.id'), "ITRACKER", "Tracking script not loaded")

            # are trackeroptions present?
            self.assertEqual(self.driver.execute_script('return window._trackeropt.uasignature'), True, "Tracker options not set")
            self.assertEqual(self.driver.execute_script('return window._trackeropt.visibility'), True, "Tracker options not set")
            self.assertEqual(self.driver.execute_script('return window._trackeropt.media'), True, "Tracker options not set")
            self.assertEqual(self.driver.execute_script('return window._trackeropt.scroll'), True, "Tracker options not set")
            self.assertEqual(self.driver.execute_script('return window._trackeropt.mouse'), True, "Tracker options not set")
            self.assertEqual(self.driver.execute_script('return window._trackeropt.touch'), True, "Tracker options not set")
            self.assertEqual(self.driver.execute_script('return window._trackeropt.interaction'), True, "Tracker options not set")
            self.assertEqual(self.driver.execute_script('return window._trackeropt.tab'), True, "Tracker options not set")
            self.assertEqual(self.driver.execute_script('return window._trackeropt.tvnow'), True, "Tracker options not set")
            self.assertEqual(self.driver.execute_script('return window._trackeropt.interval'), 1000, "Tracker options not set")
            self.assertEqual(self.driver.execute_script('return window._trackeropt.dampeningTime'), 100, "Tracker options not set")
            self.assertEqual(self.driver.execute_script('return window._trackeropt.domain'), "127.0.0.1", "Tracker options not set")

            # tracker started successfully?
            self.assertEqual(self.driver.execute_script('return window._itracker.paused'), False, "Tracker has not been started")



        with self.subTest("test_interaction_events"):
            # change (typing into edit box)
            del self.driver.requests
            self.driver.find_element(By.ID,'q2').send_keys('Testtext')
            self.driver.find_element(By.ID,'q1').click() # change focus

            self.waitAndTestRepeatedly(["change"], 3)
            self.assertMessageSyntax(["change", 0, "element-id", "INPUT"])
            
            
            # click
            del self.driver.requests
            self.driver.find_element(By.ID,'q1').click()
            self.waitAndTest("click")        
            self.assertMessageSyntax(["click", 0, "element-id", "BUTTON"])

            # dblclick
            del self.driver.requests
            webdriver.ActionChains(self.driver).double_click(self.driver.find_element(By.ID,'q1')).perform()
            self.waitAndTest("dblclick")        
            self.assertMessageSyntax(["dblclick", 0, "element-id", "BUTTON"])

            # contextmenu
            del self.driver.requests
            webdriver.ActionChains(self.driver).context_click(self.driver.find_element(By.ID,'q1')).perform()
            self.waitAndTest("context")        
            self.assertMessageSyntax(["context", 0, "element-id", "BUTTON"])

            # toggle
            del self.driver.requests
            self.driver.find_element(By.ID,'ch1').click()
            self.waitAndTest("click")             
            self.assertMessageSyntax(["click", 0, "element-id", "DETAILS"])

            # select
            # TODO

            # submit
            del self.driver.requests
            self.driver.find_element(By.ID, 'f1').click()
            self.waitAndTest("submit")  
            self.assertMessageSyntax(["submit", 0, "element-id", "FORM"])

        with self.subTest("test_visibility_event"):
            # open tab
            start_tab = self.driver.current_window_handle
            del self.driver.requests        
            self.driver.execute_script('window.open("https://www.google.com", "_blank");')

            # tab is now in background... scripts can take a little longer
            self.waitAndTestRepeatedly(["background"], 5)
            self.assertMessageSyntax(["background", 0])

            # close tab
            del self.driver.requests
            self.driver.switch_to.window(start_tab)

            self.waitAndTestRepeatedly(["foreground"], 5)
            self.assertMessageSyntax(["foreground", 0])


        with self.subTest("test_logging"):
            # log an event manually
            self.driver.execute_script('window._itracker.log("unittest",[]);')
            del self.driver.requests
            self.waitAndTest("unittest")

        with self.subTest("test_mouse_events"):
            # click, mousup, mousedown events
            del self.driver.requests
            self.driver.find_element(By.ID,'q1').click()
            self.waitAndTest("click")
            self.assertMessageSyntax(["click", 0, "element-id", "BUTTON"])
            msg = self.getMessage(ENDPOINTURL)
            self.assertMessagePresent(msg.body, "mouseup")
            self.assertMessagePresent(msg.body, "mousedown")
            
            # mouse move event
            del self.driver.requests
            webdriver.ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID,'q2')).perform()
            self.waitAndTest("mousemove")

            # mouse leave, enter
            del self.driver.requests
            webdriver.ActionChains(self.driver).move_to_element(self.driver.find_element(By.ID,'q1')).perform()
            self.waitAndTest("mouseover")
            msg = self.getMessage(ENDPOINTURL)
            self.assertMessagePresent(msg.body, "mouseenter")
            self.assertMessagePresent(msg.body, "mouseleave")
            self.assertMessagePresent(msg.body, "mouseover")
            self.assertMessagePresent(msg.body, "mouseout")   

            # wheel event
            # TODO       

        #with self.subTest("test_touch_events"):     

        with self.subTest("test_media_events"):  
            # play
            del self.driver.requests
            self.driver.execute_script("document.getElementById('vid1').play();")
            self.waitAndTest("play")
            self.assertMessageSyntax(["play", 0 ]) # , 0.0, "media-id", "video.mp4"])
            time.sleep(2)
           

            # pause
            del self.driver.requests
            self.driver.execute_script("document.getElementById('vid1').pause();")
            self.waitAndTest("pause")

            time.sleep(1)

            # seeked
            del self.driver.requests
            self.driver.execute_script("document.getElementById('vid1').currentTime = '29';")
            self.waitAndTest("seeked")   

            time.sleep(1)

            # play
            del self.driver.requests
            self.driver.execute_script("document.getElementById('vid1').play();")

            # is ended and playend fired?
            # use next eventS bc. no guarantee event is in next transmission
            self.waitAndTestRepeatedly(["ended", "playend"], 5)




        with self.subTest("test_scroll_event"):             
            del self.driver.requests
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            self.waitAndTest("scroll")
            self.assertMessageSyntax(["scroll", 0, 0])
            self.driver.implicitly_wait(5)

            del self.driver.requests
            self.driver.execute_script("window.scrollTo(0, 0);")
            self.waitAndTest("scroll")    
            self.assertMessageSyntax(["scroll", 0, 0, 0])

        with self.subTest("test_evisibility_event"):          
            del self.driver.requests
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            self.waitAndTest("evisibility")
            self.assertMessageSyntax(["evisibility", 0 ]) # 0.0, "element-id", "IMG"])
            self.driver.implicitly_wait(5)

            del self.driver.requests
            self.driver.execute_script("window.scrollTo(0, 0);")
            self.waitAndTest("evisibility") 
            self.assertMessageSyntax(["evisibility", 0 ]) # , 1.0, "element-id", "IMG"])




        #with self.subTest("test_unload_event"):          
        #    start_tab = self.driver.current_window_handle            
        #    self.driver.execute_script('window.open("https://www.google.com", "_blank");')
        #    del self.driver.requests        
        #    self.driver.switch_to.window(start_tab)    
        #    self.driver.implicitly_wait(2)        
        #    self.driver.close()
        #    #self.waitAndTest("leave")      
        #
        #    for i in range(5):
        #        try:
        #            
        #            msg = self.getMessage(ENDPOINTURL)
        #            print(msg.body)
        #            
        #        except:
        #            print("no message")
        #            pass
        #
        #        del self.driver.requests                        
        #        try:
        #            self.driver.wait_for_request(ENDPOINTURL, 2)
        #        except:
        #            pass




if __name__ == "__main__":
    unittest.main()
