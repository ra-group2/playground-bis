// You can edit this code!
// Click here and start typing.
package main

import "fmt"

func main() {
	n := 0
	for n < 10000 {
		n *= 2
		fmt.Println(n)
	}
}
